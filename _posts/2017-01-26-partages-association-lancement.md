---
layout: post
title: "Partages, association, lancement du site web"
date: 2017-01-26
---

Bien. Finalement, cette association vient de naitre et vogue maintenant sur la toile. Elle est propulsée par [Jekyll](http://jekyllrb.com) et utilise la -très simple- syntaxe Markdown pour rédiger les articles du blog. 
